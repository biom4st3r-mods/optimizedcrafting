package com.biom4st3r.recipecache.mixin;

import java.util.Optional;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.CraftingResultInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.packet.s2c.play.ScreenHandlerSlotUpdateS2CPacket;
import net.minecraft.recipe.CraftingRecipe;
import net.minecraft.recipe.RecipeType;
import net.minecraft.screen.AbstractRecipeScreenHandler;
import net.minecraft.screen.PlayerScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.server.network.ServerPlayerEntity;

@Mixin(PlayerScreenHandler.class)
public abstract class PlayerScreenHandlerMxn extends AbstractRecipeScreenHandler<CraftingInventory> {

    public PlayerScreenHandlerMxn(ScreenHandlerType<?> screenHandlerType, int i) {
        super(screenHandlerType, i);
    }
    
    @Shadow @Final
    private PlayerEntity owner;
    @Shadow @Final
    private CraftingInventory craftingInput;
    @Shadow @Final
    private CraftingResultInventory craftingResult;

    @Unique
    Optional<CraftingRecipe> lastRecipe = Optional.empty();

    
    @Inject(at = @At("HEAD"), method = "onContentChanged",cancellable = true)
    private void cachedUpdate(CallbackInfo ci)
    {
        if(!owner.world.isClient)
        {
            ItemStack stack = ItemStack.EMPTY;
            ServerPlayerEntity spe = (ServerPlayerEntity) owner;
            if(!this.craftingInput.isEmpty())
            {
                if(!this.lastRecipe.isPresent() || !this.lastRecipe.get().matches(craftingInput, owner.world) )
                {
                    this.lastRecipe = owner.world.getServer().getRecipeManager().getFirstMatch(RecipeType.CRAFTING, craftingInput, owner.world);
                }
                if(this.lastRecipe.isPresent())
                {
                    CraftingRecipe recipe = lastRecipe.get();
                    if(this.craftingResult.shouldCraftRecipe(owner.world, spe, recipe))
                    {
                        stack = recipe.craft(this.craftingInput);
                    }
                }
            }
            this.craftingResult.setStack(0, stack);
            spe.networkHandler.sendPacket(new ScreenHandlerSlotUpdateS2CPacket(this.syncId, 0, stack));
        }
        ci.cancel();
    }
}
