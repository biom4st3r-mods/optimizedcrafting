package com.biom4st3r.recipecache.mixin;

import java.util.Optional;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import net.minecraft.block.entity.AbstractFurnaceBlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.LockableContainerBlockEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.recipe.RecipeType;
import net.minecraft.world.World;

@Mixin(AbstractFurnaceBlockEntity.class)
public abstract class AbstractFurnaceBlockEntityMxn extends LockableContainerBlockEntity {

    protected AbstractFurnaceBlockEntityMxn(BlockEntityType<?> blockEntityType) {
		super(blockEntityType);
    }
    
    @Unique
    Optional< Recipe<Inventory>> lastRecipe = Optional.empty();

    @Redirect(
        at = @At(
            value = "INVOKE",
            target = "net/minecraft/recipe/RecipeManager.getFirstMatch(Lnet/minecraft/recipe/RecipeType;Lnet/minecraft/inventory/Inventory;Lnet/minecraft/world/World;)Ljava/util/Optional;"
            ),
        method = "tick"
    )
    private Optional<Recipe<Inventory>> fast_getFirstMatch(RecipeManager manager ,RecipeType<Recipe<Inventory>> type, Inventory inv, World world)
    {
        if(!lastRecipe.isPresent() || !lastRecipe.get().matches(inv, world))
        {
            lastRecipe = manager.getFirstMatch(type, inv, world);
        }

        return lastRecipe;
    }
}
